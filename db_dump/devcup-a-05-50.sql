-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2014 at 11:50 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `devcup`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `acc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_username` varchar(150) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('user','admin','dev') NOT NULL DEFAULT 'user',
  `acc_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  `acc_fb_token` varchar(200) NOT NULL,
  `acc_fb_secret` varchar(200) NOT NULL,
  `acc_fb_id` varchar(200) NOT NULL,
  `acc_photo` varchar(200) NOT NULL,
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `username` (`acc_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`, `acc_fb_token`, `acc_fb_secret`, `acc_fb_id`, `acc_photo`) VALUES
(1, 'developer@zeaple.com', 'c35c3883065eb7aac03d0c9423f26ecb', 'Developer', 'Zeaple', 'dev', 0, 'active', '', '', '', ''),
(2, 'yanna.diavane', '', 'Diavane', 'Alyanna', 'user', 0, 'active', '', '', '1402389801', 'https://graph.facebook.com/1402389801/picture'),
(3, 'hyubs', '', 'Ursua', 'Hyubs', 'user', 0, 'active', '', '', '560933402', 'https://graph.facebook.com/560933402/picture'),
(4, 'rj.s.aquino', '', 'Aquino', 'RJ', 'user', 0, 'active', '', '', '620193137', 'https://graph.facebook.com/620193137/picture'),
(5, 'jonconanan', '', 'Conanan', 'Jonathan', 'user', 0, 'active', '', '', '740130072', 'https://graph.facebook.com/740130072/picture'),
(7, 'mauricio.roderick', '', 'Mauricio', 'Roderick', 'user', 0, 'active', '', '', '100000664743178', 'https://graph.facebook.com/100000664743178/picture'),
(8, 'tetsusoujiro', '', 'Senosa', 'JM', 'user', 0, 'active', '', '', '100004223492319', 'https://graph.facebook.com/100004223492319/picture');

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `act_id` int(11) NOT NULL,
  `act_sender_fb_id` varchar(200) NOT NULL,
  `act_receiver_fb_id` varchar(200) NOT NULL,
  `act_type` enum('action 1','action 2','action 3','action 4') NOT NULL,
  `act_action_msg` varchar(200) NOT NULL,
  `act_personal_msg` text NOT NULL,
  `act_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE IF NOT EXISTS `friend` (
  `fri_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fri_fb_id1` varchar(200) NOT NULL,
  `fri_fb_id2` varchar(200) NOT NULL,
  PRIMARY KEY (`fri_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `friend`
--

INSERT INTO `friend` (`fri_id`, `fri_fb_id1`, `fri_fb_id2`) VALUES
(1, '1402389801', '560933402'),
(2, '1402389801', '620193137'),
(3, '1402389801', '740130072'),
(4, '1402389801', '100000664743178'),
(5, '1402389801', '100004223492319'),
(6, '560933402', '1402389801'),
(7, '560933402', '620193137'),
(8, '560933402', '740130072'),
(9, '560933402', '100000664743178'),
(10, '560933402', '100004223492319'),
(11, '620193137', '1402389801'),
(12, '620193137', '560933402'),
(13, '620193137', '740130072'),
(14, '620193137', '100000664743178'),
(15, '620193137', '100004223492319'),
(16, '740130072', '1402389801'),
(17, '740130072', '560933402'),
(18, '740130072', '620193137'),
(19, '740130072', '100000664743178'),
(20, '740130072', '100004223492319'),
(21, '100000664743178', '1402389801'),
(22, '100000664743178', '560933402'),
(23, '100000664743178', '620193137'),
(24, '100000664743178', '740130072'),
(25, '100000664743178', '100004223492319'),
(26, '100004223492319', '1402389801'),
(27, '100004223492319', '560933402'),
(28, '100004223492319', '620193137'),
(29, '100004223492319', '740130072'),
(30, '100004223492319', '100000664743178');

-- --------------------------------------------------------

--
-- Table structure for table `moods`
--

CREATE TABLE IF NOT EXISTS `moods` (
  `moo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pos_fb_id` varchar(200) NOT NULL,
  `moo_date` datetime NOT NULL,
  `moo_score` int(10) NOT NULL,
  `acc_fb_id` varchar(200) NOT NULL,
  PRIMARY KEY (`moo_id`),
  UNIQUE KEY `pos_fb_id` (`pos_fb_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `moods`
--

INSERT INTO `moods` (`moo_id`, `pos_fb_id`, `moo_date`, `moo_score`, `acc_fb_id`) VALUES
(1, '123456', '2014-08-16 17:49:35', 3, '1402389801'),
(2, '12131121', '2014-08-16 17:49:35', -2, '560933402'),
(3, '1213121415', '2014-08-16 17:49:36', -3, '620193137'),
(4, '523234132', '2014-08-16 17:49:36', 0, '620193137'),
(5, '121323412312', '2014-08-16 17:49:36', 0, '740130072'),
(6, '121345251235658', '2014-08-16 17:49:36', 0, '100004223492319'),
(7, '656342356547', '2014-08-16 17:49:36', 0, '100000664743178'),
(8, '92091028014039', '2014-08-16 17:49:36', -3, '100000664743178'),
(9, '3924981293812', '2014-08-16 17:49:36', 0, '1402389801'),
(10, '12142934898301', '2014-08-16 17:49:36', 4, '1402389801'),
(11, '12149719288', '2014-08-16 17:49:36', 0, '1402389801'),
(12, '999999999', '2014-08-16 17:49:36', 0, '1402389801'),
(13, '3573489572384', '2014-08-16 17:49:36', 0, '1402389801'),
(14, '2398274874', '2014-08-16 17:49:37', -3, '1402389801'),
(15, '3891274917294', '2014-08-16 17:49:37', 0, '1402389801'),
(16, '982391246298372', '2014-08-16 17:49:37', 0, '1402389801'),
(17, '2901742389579', '2014-08-16 17:49:37', -3, '1402389801'),
(18, '1291030214901', '2014-08-16 17:49:37', -3, '1402389801'),
(19, '03480923912812', '2014-08-16 17:49:38', -1, '1402389801'),
(20, '01293127491284', '2014-08-16 17:49:38', 0, '620193137'),
(21, '4859435820349', '2014-08-16 17:49:38', -4, '620193137'),
(22, '8902138821478927', '2014-08-16 17:49:38', -3, '620193137'),
(23, '842394745232232', '2014-08-16 17:49:38', -3, '620193137'),
(24, '2312948712987', '2014-08-16 17:49:38', 0, '740130072'),
(25, '40982354854297', '2014-08-16 17:49:38', 0, '740130072'),
(26, '5797457934572875', '2014-08-16 17:49:38', -2, '100004223492319'),
(27, '47985793733', '2014-08-16 17:49:38', -3, '100004223492319'),
(28, '79870980980988', '2014-08-16 17:49:38', 0, '100004223492319'),
(29, '584937593486845', '2014-08-16 17:49:38', -3, '100004223492319'),
(30, '874956958734859345', '2014-08-16 17:49:38', 0, '100004223492319'),
(31, '49893857346845', '2014-08-16 17:49:38', 3, '100004223492319'),
(32, '3947759437519238', '2014-08-16 17:49:38', 0, '100004223492319'),
(33, '4385973457834922332', '2014-08-16 17:49:38', -3, '100004223492319'),
(34, '49759358679475823094', '2014-08-16 17:49:38', -1, '100004223492319'),
(35, '98674095802394', '2014-08-16 17:49:39', 0, '100000664743178'),
(36, '38957923742084', '2014-08-16 17:49:39', 4, '100000664743178'),
(37, '573948577234012984', '2014-08-16 17:49:39', 0, '100000664743178'),
(38, '249827471203', '2014-08-16 17:49:39', 3, '100000664743178'),
(39, '423984723849211920', '2014-08-16 17:49:39', 0, '100000664743178'),
(40, '7498795238941029', '2014-08-16 17:49:39', 1, '100000664743178'),
(41, '43976945761230193', '2014-08-16 17:49:39', -5, '100000664743178'),
(42, '2255312121665', '2014-08-16 17:49:39', 0, '100000664743178'),
(43, '8976543456789', '2014-08-16 17:49:39', 0, '100000664743178'),
(44, '3479389476983', '2014-08-16 17:49:39', 0, '740130072'),
(45, '3984712038018412', '2014-08-16 17:49:39', 0, '740130072'),
(46, '4567895456789000000', '2014-08-16 17:49:39', -2, '740130072'),
(47, '34567896666444466', '2014-08-16 17:49:39', 4, '740130072'),
(48, '913085032840192', '2014-08-16 17:49:39', 3, '740130072'),
(49, '122222222222222222111111111', '2014-08-16 17:49:39', 1, '740130072'),
(50, '230910291298301921408293', '2014-08-16 17:49:39', -2, '740130072'),
(51, '32725302391052938', '2014-08-16 17:49:39', -1, '740130072');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) unsigned DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`pag_id`),
  UNIQUE KEY `slug` (`pag_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE IF NOT EXISTS `page_category` (
  `pct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pct_name` varchar(50) NOT NULL,
  PRIMARY KEY (`pct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_album`
--

CREATE TABLE IF NOT EXISTS `photo_album` (
  `alb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alb_name` varchar(50) NOT NULL,
  `alb_description` text NOT NULL,
  `alb_slug` varchar(80) NOT NULL,
  PRIMARY KEY (`alb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_fb_id` varchar(200) NOT NULL,
  `pos_url` text NOT NULL,
  `pos_fb_id` varchar(200) NOT NULL,
  `pos_text` text NOT NULL,
  `pos_is_analyzed` enum('no','yes') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`pos_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`pos_id`, `acc_fb_id`, `pos_url`, `pos_fb_id`, `pos_text`, `pos_is_analyzed`) VALUES
(1, '1402389801', 'http://www.google.com', '123456', 'I am happy!', 'yes'),
(2, '560933402', 'http://www.google.com', '12131121', 'I am sad', 'yes'),
(3, '620193137', 'http://www.google.com', '1213121415', 'I am angry.', 'yes'),
(4, '620193137', 'http://www.google.com', '523234132', 'GRAAAH', 'yes'),
(5, '740130072', 'http://www.facebook.com', '121323412312', 'HUHUHU', 'yes'),
(6, '100004223492319', 'http://www.facebook.com', '121345251235658', 'YAY!', 'yes'),
(7, '100000664743178', 'http://www.facebook.com', '656342356547', 'I really do not understand this.', 'yes'),
(8, '100000664743178', 'http://www.facebook.com', '92091028014039', 'I HATE THIIIIIIIIIIIS', 'yes'),
(9, '1402389801', 'http://www.google.com', '3924981293812', 'I AM SO HAPPYYYYY <3', 'yes'),
(10, '1402389801', 'http://www.facebook.com', '12142934898301', 'This is so fun~', 'yes'),
(11, '1402389801', 'http://www.google.com', '12149719288', 'LALALALALALA', 'yes'),
(12, '1402389801', 'http://www.google.com', '999999999', 'noninonino~', 'yes'),
(13, '1402389801', 'http://www.google.com', '3573489572384', 'Hooray for today!', 'yes'),
(14, '1402389801', 'http://www.google.com', '2398274874', 'I HATE THIS~', 'yes'),
(15, '1402389801', 'http://www.google.com', '3891274917294', 'This is so fluffy i''m gonna dieeeeee', 'yes'),
(16, '1402389801', 'http://www.google.com', '982391246298372', 'I''M GONNA DIEEEEE', 'yes'),
(17, '1402389801', 'http://www.google.com', '2901742389579', 'I really really suck :(', 'yes'),
(18, '1402389801', 'http://www.google.com', '1291030214901', 'I hate you', 'yes'),
(19, '1402389801', 'http://www.google.com', '03480923912812', 'We can''t stop~', 'yes'),
(20, '620193137', 'http://www.google.com', '01293127491284', 'I am so saaaaaaaaaad. :(', 'yes'),
(21, '620193137', 'http://www.google.com', '4859435820349', 'Fuck this~', 'yes'),
(22, '620193137', 'http://www.google.com', '8902138821478927', 'I hate youuuuuuuu', 'yes'),
(23, '620193137', 'http://www.google.com', '842394745232232', 'I hate this liiiiiife', 'yes'),
(24, '740130072', 'http://www.google.com', '2312948712987', 'Ayoko na pls :((', 'yes'),
(25, '740130072', 'http://www.google.com', '40982354854297', 'AHUHUHUHUHU <//3', 'yes'),
(26, '100004223492319', 'http://www.google.com', '5797457934572875', 'I am so depressed.', 'yes'),
(27, '100004223492319', 'http://www.google.com', '47985793733', 'I hate life.', 'yes'),
(28, '100004223492319', 'http://www.google.com', '79870980980988', ':''(', 'yes'),
(29, '100004223492319', 'http://www.google.com', '584937593486845', '</3 I hate you', 'yes'),
(30, '100004223492319', 'http://www.google.com', '874956958734859345', 'I wish my heart was numb. :(', 'yes'),
(31, '100004223492319', 'http://www.google.com', '49893857346845', 'LOL', 'yes'),
(32, '100004223492319', 'http://www.google.com', '3947759437519238', 'Ahehehe :P', 'yes'),
(33, '100004223492319', 'http://www.google.com', '4385973457834922332', 'Can I die nao?', 'yes'),
(34, '100004223492319', 'http://www.google.com', '49759358679475823094', 'I cry :((', 'yes'),
(35, '100000664743178', 'http://www.google.com', '98674095802394', 'Definitely watching X-Men days of future past', 'yes'),
(36, '100000664743178', 'http://www.google.com', '38957923742084', 'Had fun with you guys! @hkdjfsl', 'yes'),
(37, '100000664743178', 'http://www.google.com', '573948577234012984', 'NP: Jetlag', 'yes'),
(38, '100000664743178', 'http://www.google.com', '249827471203', 'Not exactly the best day ever.', 'yes'),
(39, '100000664743178', 'http://www.google.com', '423984723849211920', 'This is so gaaaaay', 'yes'),
(40, '100000664743178', 'http://www.google.com', '7498795238941029', 'YES WALANG PASOK!!!', 'yes'),
(41, '100000664743178', 'http://www.google.com', '43976945761230193', 'This is a fucking waste of time', 'yes'),
(42, '100000664743178', 'http://www.google.com', '2255312121665', '4/5 stars for The Fault in our Stars! *_*', 'yes'),
(43, '100000664743178', 'http://www.google.com', '8976543456789', 'Ang gwapo niyaaaaaaaaaaa', 'yes'),
(44, '740130072', 'http://www.google.com', '3479389476983', 'Ang lala ng traffic >:(', 'yes'),
(45, '740130072', 'http://www.google.com', '3984712038018412', 'BAKIT ANG TAGAL????', 'yes'),
(46, '740130072', 'http://www.google.com', '4567895456789000000', 'No. Just no.', 'yes'),
(47, '740130072', 'http://www.google.com', '34567896666444466', 'Definitely not doing that again. *feeling grateful* #100HappyDays', 'yes'),
(48, '740130072', 'http://www.google.com', '913085032840192', '^_^ Today''s a beautiful day!', 'yes'),
(49, '740130072', 'http://www.google.com', '122222222222222222111111111', 'I''m pretty sure that that''s not how it''s supposed to work. :/', 'yes'),
(50, '740130072', 'http://www.google.com', '230910291298301921408293', 'Ignore. Ignore.', 'yes'),
(51, '740130072', 'http://www.google.com', '32725302391052938', 'Cry me a river~ #np', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('26e96969df3963dbf452cc1efcb083d0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1408180364, ''),
('3eb1d7ec78f54ac1e2c750427e14279d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1408181545, 'a:1:{s:9:"user_data";s:0:"";}');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
