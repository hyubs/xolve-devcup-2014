<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="row">
			<div class="col-xs-3 text-center">
				<div style="background-image: url(http://graph.facebook.com/<?php echo $this->session->userdata('acc_fb_id'); ?>/picture?type=large)" class="profile-photo"></div>
				<div class="clearfix"></div>
				<span class="profile-name"><?php echo $self->acc_first_name; ?> <?php echo $self->acc_last_name; ?></span>
			</div>
			<div class="col-xs-9">
				<div>
					<canvas id="myChart" width="550" height="400"></canvas>
				</div>
				<br><br>
				<?php if ($posts->num_rows() > 0) { ?>
					<?php foreach($posts->result() as $selfpost) { ?>
						<div class="post-text">
							<?php echo htmlentities($selfpost->pos_text); ?>
							<div>
								<?php 

								if ($selfpost->moo_score <= -3) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (-3 < $selfpost->moo_score && $selfpost->moo_score <= -1) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (-1 < $selfpost->moo_score && $selfpost->moo_score <= 1) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (1 < $selfpost->moo_score && $selfpost->moo_score <= 3) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (3 < $selfpost->moo_score) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<?php
								}
								?>
							</div>
						</div>
					<?php } ?>
				<?php } else { ?>
				<div class="text-center text-muted">
					<h4>No posts yet!</h4>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<script>
$(function() {      
	var data = {
	    labels: <?php echo json_encode($moodscore['date']); ?>,
	    datasets: [
	        {
	            label: "Moods for the Week",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: <?php echo json_encode($moodscore['score']); ?>
	        }
	    ]
	};
	// Get the context of the canvas element we want to select
	var ctx = document.getElementById("myChart").getContext("2d");
	var myLineChart = new Chart(ctx).Line(data);
});

</script>