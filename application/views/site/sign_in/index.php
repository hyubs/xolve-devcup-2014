<div class="text-center">
	<h3 class="tagline">Empathy for social networks</h3>
	<br>
	<a class="btn btn-primary btn-fb" href="<?php echo (!$this->access_control->check_logged_in()) ? site_url("sign_in/connect_facebook") : site_url("sign_in/logout") ; ?>" role="button"><?php echo (!$this->access_control->check_logged_in()) ? "Sign In with Facebook" : "Logout" ; ?></a>
</div>