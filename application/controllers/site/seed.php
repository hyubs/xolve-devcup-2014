<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seed extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
	}
	
	public function index() 
	{ 

		$accounts = $this->db->get('account')->result();

		foreach ($accounts as $account) {
			if ($account->acc_fb_id) {
				$fbId1 = $account->acc_fb_id;
				for ($i = 0; $i < count($accounts); ++$i) {
					$fbId2 = $accounts[$i]->acc_fb_id;

					if ($fbId2 && $fbId1 != $fbId2) {
						$this->db->insert('friend', array(
							'fri_fb_id1' => $fbId1,
							'fri_fb_id2' => $fbId2
						));
					}
				}
			}
		}
	}
}
