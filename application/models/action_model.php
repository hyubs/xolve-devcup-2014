<?php
// Extend Base_model instead of CI_model
class Action_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('act_id', 'act_sender_fb_id', 'act_receiver_fb_id', 'act_type', 'act_type', 'act_action_msg','act_personal_msg');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('action', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function get_one($id)
	{
		$this->db->join('account', "account.acc_fb_id = {$this->table}.act_sender_fb_id");
		return parent::get_one($id);
	}

	public function get_all($params = array())
	{				
		$this->db->join('account', "account.acc_fb_id = {$this->table}.act_sender_fb_id");
		return parent::get_all($params);
	}
}