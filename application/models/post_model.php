<?php
// Extend Base_model instead of CI_model
class Post_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('pos_id', 'acc_fb_id', 'pos_fb_id', 'pos_url', 'pos_text', 'pos_is_analyzed', 'pos_date');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('post', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function get_one($id)
	{				
		$this->db->join('account', "account.acc_fb_id = {$this->table}.acc_fb_id");
		$this->db->join('moods', "moods.pos_fb_id = {$this->table}.pos_fb_id");
		return parent::get_one($id);
	}

	public function get_all($params = array())
	{				
		$this->db->join('account', "account.acc_fb_id = {$this->table}.acc_fb_id");
		$this->db->join('moods', "moods.pos_fb_id = {$this->table}.pos_fb_id");
		return parent::get_all($params);
	}
}