﻿<!doctype html>
<html lang="en">
<head>
	<title><?php echo template('title'); ?> | cheers</title>
	<meta charset="utf-8">	
	<?php echo template('mythos'); ?>
	<?php echo template('bootstrap'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('site/css/styles.css'); ?>" />
	<script type="text/javascript" src="<?php echo res_url('site/js/Chart.min.js'); ?>"></script>
	<?php echo template('head'); ?>
	<script>
		// $(document).ready(function(){
		// 	$("img").error(function () {
		// 	 	$(this).unbind("error").attr("src", '<?php echo res_url("site/images/blank-profile.jpg"); ?>');
		// 	});
		// });
	</script>
</head>
<body class="<?php echo uri_css_class(); ?>">
	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">
						<img src="<?php echo res_url('site/images/logo.png'); ?>" class="logo">
					</a>
				</div>
				<ul class="nav navbar-nav navbar-left">
					<li><a href="<?php echo site_url('friends'); ?>">friends</a></li>
					<li><a href="<?php echo site_url('cheers'); ?>">cheers</a></li>
					<li><a href="<?php echo site_url('me'); ?>">me</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<div class="nav-user-photo" style="background-image: url(http://graph.facebook.com/<?php echo $this->session->userdata('acc_fb_id'); ?>/picture?type=large);"></div>
					</li>
					<li>
						<a href="<?php echo site_url('me'); ?>">
						Hello, <?php echo $this->session->userdata('acc_first_name'); ?></a>
					</li>
					<li><a href="<?php echo site_url('logout'); ?>">logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php echo template('notification'); ?>
				<?php echo template('content'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 footer">2014 &copy; cheers</div>
		</div>
	</div>
</body>
</html>