<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Facebook_library 
{
    private $CI;
    var $FB_APP;
    var $FB_SECRET;
    public function __construct() 
    {
        $this->CI =& get_instance();

        $this->FB_SECRET = $this->CI->config->item('fbsecret_id');        
        $this->FB_APP = $this->CI->config->item('fbapp_id');
        
        $this->CI->load->model("account_model");
    }

    
}