<?php
// Extend Base_model instead of CI_model
class Friend_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('fri_id', 'fri_fb_id1', 'fri_fb_id2');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('friend', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.

	public function get_all($params = array())
	{				
		$this->db->select('fri_fb_id2');
		$this->db->where("fri_fb_id1", $params['fri_fb_id1']);
		return $this->db->get($this->table);
	}
}