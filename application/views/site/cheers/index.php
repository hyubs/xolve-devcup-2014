<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
	<?php if($cheers->num_rows()): 
        foreach($cheers->result() as $cheer): ?>
        <div class="cheer-div">
        	
			<div class="row">
				<div class="col-xs-3">
					<div style="background-image: url(http://graph.facebook.com/<?php echo $cheer->acc_fb_id; ?>/picture?type=large)" class="profile-photo"></div>
				</div>
				<div class="col-xs-9">
					<div class="">
						<em class="highlight"><?php echo $cheer->act_action_msg; ?></em>
					</div>
					<hr>
					<div class="">
						<?php echo $cheer->act_personal_msg; ?>
					</div>
				</div>
			</div>
        </div>
		<?php endforeach; ?>
	<?php endif; ?>
	</div>
</div>