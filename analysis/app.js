var _         = require('lodash');
var async     = require('async');
var mysql     = require('mysql');
var sentiment = require('sentiment');
var moment = require('moment');

var db = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'devcup'
});

db.connect();


var getPost = function (callback) {
    db.query('SELECT * FROM post WHERE pos_is_analyzed = "no" LIMIT 0, 1', function(err, rows, fields) {
        if (err)  
            return callback(err);

        if (rows.length > 0)
            return callback(null, rows[0]);
        else
            return callback(null, null);
    });
};

var analysis = function (text, callback) {
    var result = sentiment(text);
    console.log(result);
    callback(null, result.score);
};

var saveAnalysis = function (fbId, posFbId, score, posDate, callback) {
    var mood = {
        acc_fb_id: fbId,
        pos_fb_id: posFbId,
        moo_date: moment(posDate).format('YYYY-MM-DD HH:mm:ss'),
        moo_score: score
    };

    db.query('INSERT INTO moods SET ?', mood, function(err) {
        // if (err)
        //     return callback(err);



        db.query('UPDATE post SET pos_is_analyzed = "yes" WHERE pos_fb_id = "' + posFbId + '"', function(err) {
            if (err)
                return callback(err);

            return callback();
        });
    });
};

async.forever(
    function(next) {
        getPost(function (err, post) {
            if (err)
                return next(err);

            if (post) {
                analysis(post.pos_text, function (err, score) {
                    if (err)
                        return next(err);

                    saveAnalysis(post.acc_fb_id, post.pos_fb_id, score, post.pos_date, function (err) {
                        if (err)
                            return next(err);
                        
                        next();
                    });
                });
            } else {
                process.nextTick(function () {
                    next();
                });
            }
        });
    },
    function(err) {
        console.error(err);
        process.exit(0);
    }
);


// db.end();