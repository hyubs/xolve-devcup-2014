
<div class="row">
	<div class="col-sm-12">
		<canvas id="myChart" width="800" height="300" style="margin: 0 auto; display: block;"></canvas>
	</div>
</div>
<div class="row">	
	<?php if (!empty($moods)){ ?>
		<?php $count = 0; foreach($moods as $mood): ?>
		<div class="col-sm-3">
			<a href="<?php echo site_url('friends/view/'.$mood['acc_id']); ?>" class="friend">
				<div style="background-image: url(http://graph.facebook.com/<?php echo $mood['acc_fb_id'] ?>/picture?type=large)" class="friend-photo"></div>
				<div class="center-block"><?php echo $mood['acc_first_name'] ?> <?php echo $mood['acc_last_name']; ?></div>
				
				<?php
					if ($mood['moo_score'] <= -3) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (-3 < $mood['moo_score'] && $mood['moo_score'] <= -1) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (-1 < $mood['moo_score'] && $mood['moo_score'] <= 1) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (1 < $mood['moo_score'] && $mood['moo_score'] <= 3) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (3 < $mood['moo_score']) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<?php
					}
				?>
			</a>
		</div>
		<?php $count++; endforeach; ?> 		
	<?php }else{ ?>
		<div class="col-sm-3">
			oooOOPPSS!!! 
		</div>
	<?php } ?>
</div>
<script>
$(function() {      
	var data = {
	    labels: <?php echo json_encode($moodscore['date']); ?>,
	    datasets: [
	        {
	            label: "Moods for the Week",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: <?php echo json_encode($moodscore['score']); ?>
	        }
	    ]
	};
	// Get the context of the canvas element we want to select
	var ctx = document.getElementById("myChart").getContext("2d");
	var myLineChart = new Chart(ctx).Line(data);
});
</script>