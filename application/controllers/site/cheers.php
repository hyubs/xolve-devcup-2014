<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cheers extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->model('action_model');
		$this->load->helper('format');
	}
	
	public function index() 
	{
		$fb_id = $this->session->userdata('acc_fb_id');
		$cheers = $this->action_model->get_all( array('act_receiver_fb_id' => $fb_id) );

		$page_params = array();
		$page_params['cheers'] = $cheers;
	
		$this->template->title('cheers');
		
		$this->template->content('cheers-index', $page_params);
		
		$this->template->show('site');
	}
}
