﻿<!doctype html>
<html lang="en">
<head>
	<title><?php echo template('title'); ?> | cheers</title>
	<meta charset="utf-8">	
	<?php echo template('mythos'); ?>
	<?php echo template('bootstrap'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('site/css/styles.css'); ?>" />
	<?php echo template('head'); ?>
</head>
<body class="<?php echo uri_css_class(); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php echo template('notification'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 text-center">
				<img src="<?php echo res_url('site/images/logo.png'); ?>" alt="" style="margin-top: 128px;">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<?php echo template('content'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 footer">2014 &copy; cheers</div>
		</div>
	</div>
</body>
</html>