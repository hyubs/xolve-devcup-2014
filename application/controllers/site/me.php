<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Me extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('account_model');
		$this->load->model('post_model');
		$this->load->model('moods_model');
		$this->load->helper('format');
	}
	
	public function index() 
	{
		$acc_id = $this->session->userdata('acc_id');
		$page_params = array();
	
		$page_params['self'] = $this->account_model->get_one($acc_id);

		$page_params['posts'] = $this->post_model->get_all(array('acc_id' => $acc_id));
		$moodscore = $this->moods_model->get_mood_scores($page_params['self']->acc_fb_id, 7);
		$graphdata = array();
		
		$moodscore = $moodscore->result();
		$graphdata['date'] = '';
		$graphdata['score'] = '';

		for($count = 0; $count < sizeof($moodscore); $count++)
		{
			$graphdata['date'][$count] = $moodscore[$count]->moo_date;
			$graphdata['score'][$count] = $moodscore[$count]->moo_score;
			
		}

		$page_params['moodscore'] = $graphdata;
		
		$this->template->title('me');
		
		$this->template->content('me-index', $page_params);
		
		$this->template->show('site');
	}

	public function view($acc_id)
	{
		$page_params = array();
		$page_params['self'] = $this->account_model->get_one($self_id);
	
		$page_params['account'] = $this->account_model->get_one($acc_id);

        $filter = array('acc_id' => $acc_id);
		$page_params['posts'] = $this->post_model->get_all($filter);

		$this->template->title();
		
		$this->template->content('friends-view', $page_params);
		
		$this->template->show('site');
	}
}
