<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="row">
			<div class="col-xs-3 text-center">
				<div style="background-image: url(http://graph.facebook.com/<?php echo $account->acc_fb_id; ?>/picture?type=large)" class="profile-photo"></div>
				<div class="clearfix"></div>
				<span class="profile-name"><?php echo $account->acc_first_name; ?> <?php echo $account->acc_last_name; ?></span>
				<div class="text-center">
					<?php 
					if ($account->acc_id == 90) 
						$moo_score = 0;

					if ($moo_score <= -3) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (-3 < $moo_score && $moo_score <= -1) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (-1 < $moo_score && $moo_score <= 1) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (1 < $ac->moo_score && $moo_score <= 3) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull empty"></span>
						<?php
					} elseif (3 < $moo_score) {
						?>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<span class="mood-bull"></span>
						<?php
					}
					?>
				</div>
				<div class="text-center">
					<br>
					<ul class="list-inline">
						<li><button class="btn btn-default cheer-action-icon cheer-action-hug" type="button" onclick="preModal('action 1');" data-toggle="modal" data-target="#myModal"></button></li>
						<li><button class="btn btn-default cheer-action-icon cheer-action-animal" type="button" onclick="preModal('action 2');" data-toggle="modal" data-target="#myModal"></button></li>
						<li><button class="btn btn-default cheer-action-icon cheer-action-hangout" type="button" onclick="preModal('action 3');" data-toggle="modal" data-target="#myModal"></button></li>
					</ul>
					<p class="text-muted"><small>Cheer your friend</small></p>
				</div>
				
			</div>
			<div class="col-xs-9">
				<div>
					<canvas id="myChart" width="550" height="400"></canvas>
				</div>
				<br><br>
				<div class="clearfix"></div>
				<?php if ($posts->num_rows() > 0) { ?>
					<?php foreach($posts->result() as $accpost) { ?>
						<div class="post-text">
							<?php echo htmlentities($accpost->pos_text); ?>
							<div>
								<?php 
								if ($accpost->moo_score <= -3) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (-3 < $accpost->moo_score && $accpost->moo_score <= -1) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (-1 < $accpost->moo_score && $accpost->moo_score <= 1) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (1 < $accpost->moo_score && $accpost->moo_score <= 3) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull empty"></span>
									<?php
								} elseif (3 < $accpost->moo_score) {
									?>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<span class="mood-bull"></span>
									<?php
								}
								?>
							</div>
						</div>
					<?php } ?>
				<?php } else { ?>
				<div class="text-center text-muted">
					<h4>No posts yet!</h4>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<form name="action" id="action" method="post">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Cheer <?php echo $account->acc_first_name; ?> <span id="cheer-text"></span></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-4 text-center">
							<img src="<?php echo base_url('uploads/images/pages/pre1.png') ?>" class="profile-photo">
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<br>
								<textarea class="form-control" id="message" name="act_personal_msg" rows="4" placeholder="Write a message..."></textarea> 
							</div>
							<input type="hidden" name="act_type" id="act_type" value="">
							<input type="submit" class="btn btn-primary" onClick="document.forms['action'].submit();" value="Send Message">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
$(function() {      
	var data = {
	    labels: <?php echo json_encode($moodscore['date']); ?>,
	    datasets: [
	        {
	            label: "Moods for the Week",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: <?php echo json_encode($moodscore['score']); ?>
	        }
	    ]
	};
	// Get the context of the canvas element we want to select
	var ctx = document.getElementById("myChart").getContext("2d");
	var myLineChart = new Chart(ctx).Line(data);
});

	function preModal(val) {
		$('#act_type').val(val);

		var text = "";
		if      (val === "action 1") text = "with a hug";
		else if (val === "action 2") text = "with pictures of cute animals";
		else if (val === "action 3") text = "by hanging out";

		$('#cheer-text').html(text);
	}
</script>