<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grab_news extends CI_Controller 
{
    var $access_token;

    public function __construct() 
    {
        parent::__construct();
        $this->load->model(array('page_model',"post_model","account_model"));
        $this->load->helper('format');
        $this->load->library("user_agent");
        $this->load->library("facebook_library"); 

        $fb_config = array(
            'appId'  => $this->config->item('fbapp_id'),
            'secret' => $this->config->item('fbsecret_id')
        );
        $this->load->library('facebook', $fb_config);

        $this->access_token = "CAACEdEose0cBAOR87GEZB9hLuJEBXh9mnkCH60T1jcswQg0vvHdSw2eUptvbaJyyQZBnesyZA08Bz3uEFpq6RZB5QJ2Fqoixtrb5CBtMGOg7nOXrvUfIyW3ML5iFQ6J5ZCwwot3z3pLGFm1OWQRBq9AXgsFfzwbxDlVzW8u6Kj0w33fZCPDR7qV2VmOLdVROrR0M2FE7io0EXo1gFtooplErTcctfsesIZD";
    }
    
    public function index($fb_id2 = 0) 
    {       
        if ($this->session->userdata("acc_fb_id")) 
            $fb_user_id = "me";
        else
            $fb_user_id = "me";

        $url = "https://graph.facebook.com/v2.1/".$fb_user_id."/home?limit=120&access_token=".$this->access_token; 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $result = curl_exec($ch); 
        $result = json_decode($result); 
        $data = $result->data;
        foreach ($data as $key => $value) { 
            
            // check if fb id is existing
            $account = $this->account_model->get_all(array("acc_fb_id"=>$value->from->id));

            // if not existing create account 
            if ($account->num_rows() == 0) 
            { 
                $account_id = $this->create_account($value); 
                if ($account_id == null) 
                {
                    continue;
                }
                $account = $this->account_model->get_one($account_id);              
                $this->link_account($fb_id2, $account->acc_fb_id);
            }
            else
            {
                $account = $account->row();
                $account_id = $account->acc_id;
            }

            $account = $this->account_model->get_one($account_id);

            $post = $this->post_model->get_all(array("post.acc_fb_id"=>$value->id));
            if ($post->num_rows() == 0) 
            {
                $post_data = array(
                        "acc_fb_id" => $account->acc_fb_id,
                        "pos_url"   => (isset($value->link)) ? $value->link : $value->actions[0]->link,
                        "pos_fb_id" => $value->id,
                        "pos_text"  => (isset($value->message)) ? @$value->message : @$value->story,
                        "pos_date"  => date("Y-m-d H:i:s", strtotime($value->created_time))
                    ); 

                $this->post_model->create($post_data); 
            }

        }
        
    }  

    public function create_account($value)
    {
        
        $name = explode(" ", $value->from->name);
        if (!isset($name[0]) && !isset($name[1])) {
            return null;   
        }

        $fb_getImg = 'http://graph.facebook.com/'.$value->from->id.'/picture?type=large&redirect=false';
        $profile_pic = json_decode(file_get_contents($fb_getImg));  
        $image_name = md5($value->from->id."-".date("Y-m-d h:i:s")).'.jpg';
        $facebook_img = file_get_contents($profile_pic->data->url);

        $username = $name[0].$name[1];                
        $account_data = array(
                "acc_username" => $name[0]."-".$name[1],
                "acc_fb_id" => $value->from->id,
                "acc_first_name" => $name[0],
                "acc_last_name"  => $name[1],
                "acc_photo"     => $profile_pic->data->url
            );
        $account_id = $this->account_model->create($account_data);

        return $account_id;
    }

    public function link_account($fb_id, $to_id = 0)
    {
   

        $this->load->model("friend_model");

        $this->friend_model->create(array("fri_fb_id1"=>$fb_id, "fri_fb_id2"=>$to_id));
        $this->friend_model->create(array("fri_fb_id1"=>$to_id,"fri_fb_id2"=>$fb_id));
    }

    public function post()
    {
        $uid = "10202920910369406"; //User's Facebook ID, set the value as "me" if publishing on logged in user's wall
        $ufname = "Alyanna Diavane"; // User's Full name on Facebook
        
        $attachment = array(
            'message'=> "Hello Yanna!!! Test lang to, $ufname!",
            'location'=> "148505625222580",
            'tags' => '10202920910369406',
            'place' => '148505625222580'
        );
        try {
            $this->facebook->setAccessToken($this->access_token);
            
            $post = $this->facebook->api('/me/feed', 'post', $attachment); 
        
        } catch (FacebookApiException $e) {
            echo "<pre>";
            print_r($e);
            exit;
        }

        $attachment =  array(
            'access_token' => $this->access_token,
            'message' => "$ufname Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            'name' => "Try lang itetchinechi!!!",
            'link' => site_url(),
            'description' => " Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'picture'=>"http://64.19.142.12/stuffpoint.com/cats/image/241312-cats-super-cute-kitty.jpg",
            'actions' => json_encode(
                array(
                        'name' => "post",
                        'link' => "http://stuffpoint.com/cats/image/241312/super-cute-kitty-picture/")
                ),
            'tags' => '10202920910369406',
            'place' => '148505625222580',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://graph.facebook.com/me/feed');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $attachment);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output 
        $result = curl_exec($ch);
        curl_close ($ch);

        print_r($result);

    }
    // https://www.google.com.ph/maps/place/Meralco+Gym/@14.5893629,121.0645422,19z/data=!4m18!1m15!4m14!1m6!1m2!1s0x3397c81a5be21fb5:0x2dcfa1f8d7afed36!2sMeralco+Gym,+Ortigas+Avenue,+Metro+Manila,+Pasig+City+1605!2m2!1d121.064678!2d14.589729!1m6!1m2!1s0x3397c81a6a81ee17:0x1bbbed3a76fd3dd0!2sMeralco+Theater,+Pasig!2m2!1d121.063768!2d14.589476!3m1!1s0x3397c81a5be21fb5:0x2dcfa1f8d7afed36
}