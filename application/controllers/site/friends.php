<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Friends extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('account_model');
		$this->load->model('post_model');
		$this->load->model('action_model');
		$this->load->model('friend_model');
		$this->load->model('moods_model');
		$this->load->helper('format');
	}
	
	public function index() 
	{
		$acc_id = $this->session->userdata('acc_id');
		$fb_id = $this->session->userdata('acc_fb_id');
		$page_params = array();
	
		$friends = $this->friend_model->get_all( array('fri_fb_id1' => $fb_id) );
		$listfriends = array();
		foreach($friends->result() as $friend)
		{
			array_push($listfriends, $friend->fri_fb_id2);
		}

		$page_params['accounts'] = $accounts = $this->account_model->get_friends($listfriends);
		$page_params['moods'] = array();

		$moodscore = $this->moods_model->get_all_mood_scores(7);
		$moodscore = (array) $moodscore->result();
		$graphdata = array();
		
		$graphdata['date'] = '';
		$graphdata['score'] = '';

		for($count = 0; $count < sizeof($moodscore); $count++)
		{
			$graphdata['date'][$count] = $moodscore[$count]->moo_date;
			$graphdata['score'][$count] = $moodscore[$count]->moo_score;
			
		}

		$page_params['moodscore'] = $graphdata;
				
				
		if ($page_params['accounts'] !== false) 
		{
			foreach($accounts->result() as $accounted)
			{
				$usermood = $this->moods_model->get_moods_from_date($accounted->acc_fb_id, 1);
				$usermood = $usermood->result();
				$usermood = (array) $usermood[0];
				$uid = $accounted->acc_id;
				$accounted = array_merge((array)$accounted, $usermood);
				$accounted['acc_id'] = $uid;
				array_push($page_params['moods'], $accounted);
			}
		
			foreach ($page_params['moods'] as $key => $row) {
			    $edition[$key] = $row['moo_score'];
			}

			// Sort the data with volume descending, edition ascending
			// Add $data as the last parameter, to sort by the common key
			array_multisort($edition, SORT_ASC, $page_params['moods']);
		}
		

		$this->template->title('friends');
		
		$this->template->content('friends-index', $page_params);
		
		$this->template->show('site');
	}

	public function view($acc_id)
	{
		$account = $this->account_model->get_one($acc_id);

		if($this->input->post())
		{
			$action = $this->extract->post();
			$action['act_sender_fb_id'] = $this->session->userdata('acc_fb_id');
		    $action['act_receiver_fb_id'] = $account->acc_fb_id;

			$action = $this->action_model->create($action);

			$newaction = $this->action_model->get_one($action);

			if($newaction->act_type == 'action 1') $act = 'hug';
			if($newaction->act_type == 'action 2') $act = 'cute animal picture';
			if($newaction->act_type == 'action 3') $act = 'invite to hang out';

			$action_message = $this->session->userdata('acc_fullname') . ' just sent you a '. $act .'!';
			$action_message = preg_replace('#\b(a)\b(?:\s(a|e|i|o|u))#i' , '$1n $2', $action_message);
			
			if($newaction->act_type == 'action 2') $action_message .= '<br>http://stuffpoint.com/cats/image/166012-cats-cats.jpg';
		    $newaction->act_action_msg = $action_message;
		    
			$this->action_model->update($newaction);		    

			$this->template->notification('You cheered ' . $account->acc_first_name . ' up!', 'success');
			redirect('cheers');
		}


		$page_params = array();
		
		$page_params['account'] = $account;

        $filter = array('acc_id' => $acc_id);

		$page_params['mood'] = $this->moods_model->get_moods_from_date($page_params['account']->acc_fb_id, 1);
		$moodscore = $this->moods_model->get_mood_scores($page_params['account']->acc_fb_id, 7);
		$graphdata = array();
		
		$moodscore = $moodscore->result();
		$graphdata['date'] = '';
		$graphdata['score'] = '';

		for($count = 0; $count < sizeof($moodscore); $count++)
		{
			$graphdata['date'][$count] = $moodscore[$count]->moo_date;
			$graphdata['score'][$count] = $moodscore[$count]->moo_score;
			
		}

		$page_params['moodscore'] = $graphdata;
		
		$page_params['posts'] = $this->post_model->get_all($filter);

		$usermood = $this->moods_model->get_moods_from_date($acc_id, 1);
		$usermood = $usermood->result();
		$page_params['moo_score'] = $usermood[0]->moo_score;

		$this->template->title($account->acc_first_name . ' ' . $account->acc_last_name);
		
		$this->template->content('friends-view', $page_params);
		
		$this->template->show('site');
	}
}
