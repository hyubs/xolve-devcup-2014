-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2014 at 11:15 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `devcup`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `acc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_username` varchar(150) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('user','admin','dev') NOT NULL DEFAULT 'user',
  `acc_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  `acc_fb_token` varchar(200) NOT NULL,
  `acc_fb_secret` varchar(200) NOT NULL,
  `acc_fb_id` varchar(200) NOT NULL,
  `acc_photo` varchar(200) NOT NULL,
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `username` (`acc_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`, `acc_fb_token`, `acc_fb_secret`, `acc_fb_id`, `acc_photo`) VALUES
(1, 'developer@zeaple.com', 'c35c3883065eb7aac03d0c9423f26ecb', 'Developer', 'Zeaple', 'dev', 0, 'active', '', '', '', ''),
(2, 'yanna.diavane', '', 'Diavane', 'Alyanna', 'user', 0, 'active', '', '', '1402389801', 'https://graph.facebook.com/1402389801/picture'),
(3, 'hyubs', '', 'Ursua', 'Hyubs', 'user', 0, 'active', '', '', '560933402', 'https://graph.facebook.com/560933402/picture'),
(4, 'rj.s.aquino', '', 'Aquino', 'RJ', 'user', 0, 'active', '', '', '620193137', 'https://graph.facebook.com/620193137/picture'),
(5, 'jonconanan', '', 'Conanan', 'Jonathan', 'user', 0, 'active', '', '', '740130072', 'https://graph.facebook.com/740130072/picture'),
(7, 'mauricio.roderick', '', 'Mauricio', 'Roderick', 'user', 0, 'active', '', '', '100000664743178', 'https://graph.facebook.com/100000664743178/picture'),
(8, 'tetsusoujiro', '', 'Senosa', 'JM', 'user', 0, 'active', '', '', '100004223492319', 'https://graph.facebook.com/100004223492319/picture');

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `act_id` int(11) NOT NULL,
  `act_sender_fb_id` varchar(200) NOT NULL,
  `act_receiver_fb_id` varchar(200) NOT NULL,
  `act_type` enum('action 1','action 2','action 3','action 4') NOT NULL,
  `act_action_msg` varchar(200) NOT NULL,
  `act_personal_msg` text NOT NULL,
  `act_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `moods`
--

CREATE TABLE IF NOT EXISTS `moods` (
  `moo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pos_fb_id` varchar(200) NOT NULL,
  `moo_date` datetime NOT NULL,
  `moo_score` int(10) NOT NULL,
  PRIMARY KEY (`moo_id`),
  UNIQUE KEY `pos_fb_id` (`pos_fb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) unsigned DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`pag_id`),
  UNIQUE KEY `slug` (`pag_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE IF NOT EXISTS `page_category` (
  `pct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pct_name` varchar(50) NOT NULL,
  PRIMARY KEY (`pct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_album`
--

CREATE TABLE IF NOT EXISTS `photo_album` (
  `alb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alb_name` varchar(50) NOT NULL,
  `alb_description` text NOT NULL,
  `alb_slug` varchar(80) NOT NULL,
  PRIMARY KEY (`alb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_fb_id` varchar(200) NOT NULL,
  `pos_url` text NOT NULL,
  `pos_fb_id` varchar(200) NOT NULL,
  `pos_text` text NOT NULL,
  `pos_is_analyzed` enum('no','yes') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`pos_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`pos_id`, `acc_fb_id`, `pos_url`, `pos_fb_id`, `pos_text`, `pos_is_analyzed`) VALUES
(1, '1402389801', 'http://www.google.com', '123456', 'I am happy!', 'no'),
(2, '560933402', 'http://www.google.com', '12131121', 'I am sad', 'no'),
(3, '620193137', 'http://www.google.com', '1213121415', 'I am angry.', 'no'),
(4, '620193137', 'http://www.google.com', '523234132', 'GRAAAH', 'no'),
(5, '740130072', 'http://www.facebook.com', '121323412312', 'HUHUHU', 'no'),
(6, '100004223492319', 'http://www.facebook.com', '121345251235658', 'YAY!', 'no'),
(7, '100000664743178', 'http://www.facebook.com', '656342356547', 'I really do not understand this.', 'no'),
(8, '100000664743178', 'http://www.facebook.com', '92091028014039', 'I HATE THIIIIIIIIIIIS', 'no'),
(9, '1402389801', 'http://www.google.com', '3924981293812', 'I AM SO HAPPYYYYY <3', 'no'),
(10, '1402389801', 'http://www.facebook.com', '12142934898301', 'This is so fun~', 'no'),
(11, '1402389801', 'http://www.google.com', '12149719288', 'LALALALALALA', 'no'),
(12, '1402389801', 'http://www.google.com', '999999999', 'noninonino~', 'no'),
(13, '1402389801', 'http://www.google.com', '3573489572384', 'Hooray for today!', 'no'),
(14, '1402389801', 'http://www.google.com', '2398274874', 'I HATE THIS~', 'no'),
(15, '1402389801', 'http://www.google.com', '3891274917294', 'This is so fluffy i''m gonna dieeeeee', 'no'),
(16, '1402389801', 'http://www.google.com', '982391246298372', 'I''M GONNA DIEEEEE', 'no'),
(17, '1402389801', 'http://www.google.com', '2901742389579', 'I really really suck :(', 'no'),
(18, '1402389801', 'http://www.google.com', '1291030214901', 'I hate you', 'no'),
(19, '1402389801', 'http://www.google.com', '03480923912812', 'We can''t stop~', 'no'),
(20, '620193137', 'http://www.google.com', '01293127491284', 'I am so saaaaaaaaaad. :(', 'no'),
(21, '620193137', 'http://www.google.com', '4859435820349', 'Fuck this~', 'no'),
(22, '620193137', 'http://www.google.com', '8902138821478927', 'I hate youuuuuuuu', 'no'),
(23, '620193137', 'http://www.google.com', '842394745232232', 'I hate this liiiiiife', 'no'),
(24, '740130072', 'http://www.google.com', '2312948712987', 'Ayoko na pls :((', 'no'),
(25, '740130072', 'http://www.google.com', '40982354854297', 'AHUHUHUHUHU <//3', ''),
(26, '100004223492319', 'http://www.google.com', '5797457934572875', 'I am so depressed.', 'no'),
(27, '100004223492319', 'http://www.google.com', '47985793733', 'I hate life.', 'no'),
(28, '100004223492319', 'http://www.google.com', '79870980980988', ':''(', 'no'),
(29, '100004223492319', 'http://www.google.com', '584937593486845', '</3 I hate you', 'no'),
(30, '100004223492319', 'http://www.google.com', '874956958734859345', 'I wish my heart was numb. :(', 'no'),
(31, '100004223492319', 'http://www.google.com', '49893857346845', 'LOL', 'no'),
(32, '100004223492319', 'http://www.google.com', '3947759437519238', 'Ahehehe :P', 'no'),
(33, '100004223492319', 'http://www.google.com', '4385973457834922332', 'Can I die nao?', 'no'),
(34, '100004223492319', 'http://www.google.com', '49759358679475823094', 'I cry :((', 'no'),
(35, '100000664743178', 'http://www.google.com', '98674095802394', 'Definitely watching X-Men days of future past', 'no'),
(36, '100000664743178', 'http://www.google.com', '38957923742084', 'Had fun with you guys! @hkdjfsl', 'no'),
(37, '100000664743178', 'http://www.google.com', '573948577234012984', 'NP: Jetlag', 'no'),
(38, '100000664743178', 'http://www.google.com', '249827471203', 'Not exactly the best day ever.', 'no'),
(39, '100000664743178', 'http://www.google.com', '423984723849211920', 'This is so gaaaaay', 'no'),
(40, '100000664743178', 'http://www.google.com', '7498795238941029', 'YES WALANG PASOK!!!', 'no'),
(41, '100000664743178', 'http://www.google.com', '43976945761230193', 'This is a fucking waste of time', 'no'),
(42, '100000664743178', 'http://www.google.com', '2255312121665', '4/5 stars for The Fault in our Stars! *_*', 'no'),
(43, '100000664743178', 'http://www.google.com', '8976543456789', 'Ang gwapo niyaaaaaaaaaaa', 'no'),
(44, '740130072', 'http://www.google.com', '3479389476983', 'Ang lala ng traffic >:(', 'no'),
(45, '740130072', 'http://www.google.com', '3984712038018412', 'BAKIT ANG TAGAL????', 'no'),
(46, '740130072', 'http://www.google.com', '4567895456789000000', 'No. Just no.', 'no'),
(47, '740130072', 'http://www.google.com', '34567896666444466', 'Definitely not doing that again. *feeling grateful* #100HappyDays', 'no'),
(48, '740130072', 'http://www.google.com', '913085032840192', '^_^ Today''s a beautiful day!', 'no'),
(49, '740130072', 'http://www.google.com', '122222222222222222111111111', 'I''m pretty sure that that''s not how it''s supposed to work. :/', 'no'),
(50, '740130072', 'http://www.google.com', '230910291298301921408293', 'Ignore. Ignore.', 'no'),
(51, '740130072', 'http://www.google.com', '32725302391052938', 'Cry me a river~ #np', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('26e96969df3963dbf452cc1efcb083d0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1408180364, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
