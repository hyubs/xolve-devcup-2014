<?php
// Extend Base_model instead of CI_model
class Moods_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('moo_id', 'acc_fb_id', 'pos_fb_id', 'moo_date', 'moo_score');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('moods', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function get_one($id)
	{				
		$this->db->join('account', "account.acc_fb_id = {$this->table}.acc_fb_id");
		return parent::get_one($id);
	}

	public function get_all($params = array())
	{
		$this->db->select_avg('moo_score');				
		$this->db->join('account', "account.acc_fb_id = {$this->table}.acc_fb_id");
		return parent::get_all($params);
	}

	public function get_moods_from_date($acc_id, $date_interval)
	{
		$query = 'SELECT AVG(`moo_score`) AS moo_score, `account`.`acc_id` FROM (`moods`) JOIN `account` ON `account`.`acc_fb_id` = `moods`.`acc_fb_id` WHERE `moods`.`acc_fb_id` LIKE "' . $acc_id . '" AND DATE(moo_date) >= DATE(DATE_SUB(NOW(), INTERVAL ' . $date_interval . ' DAY)) ORDER BY AVG(`moo_score`) ASC';
		$result = $this->db->query($query);
		return $result;	
	}

	public function get_mood_scores($id, $date_interval)
	{
		$this->db->select_avg('moo_score');				
		
		$this->db->select('DATE(moo_date) as moo_date');				
		$where = "moo_date >= DATE(DATE_SUB(NOW(), INTERVAL ".$date_interval." DAY)) AND account.acc_fb_id = ". $id;
		$this->db->where($where);
		$this->db->join('account', "account.acc_fb_id = {$this->table}.acc_fb_id");
		$this->db->group_by('DATE(moo_date)');
        $query = $this->db->get('moods');
        return $query;

	}

	public function get_all_mood_scores($date_interval)
	{
		$this->db->select_avg('moo_score');				
		
		$this->db->select('DATE(moo_date) as moo_date');				
		$where = "moo_date >= DATE(DATE_SUB(NOW(), INTERVAL ".$date_interval." DAY))";
		$this->db->where($where);
		$this->db->group_by('DATE(moo_date)');
        $query = $this->db->get('moods');
        return $query;

	}
}