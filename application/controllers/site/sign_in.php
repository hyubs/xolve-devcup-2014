<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sign_in extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->helper('format');
		$this->load->library("user_agent");
		$this->load->library("facebook_library");
	}
	
	public function index() 
	{ 

		$page_params = array();
		$this->template->title("Connect");
		
		$this->template->content('sign_in-index', $page_params);
		
		$this->template->show('site', 'templates-navless');
	}

	public function logout()
	{
		$this->session->unset_userdata("acc_id");
		$this->session->unset_userdata("acc_fb_id");
		$this->session->unset_userdata("acc_fb_token");
		$this->session->unset_userdata("acc_type");
		$this->session->unset_userdata("acc_username");

		redirect();
	}

	public function connect_facebook($request = "set")
	{
		if($this->access_control->check_logged_in())
		{
			$this->template->notification("Please logout first.", 'info');
			redirect();
		}

		$fb['fbsecret_id'] = $this->config->item('fbsecret_id');        
	    $fb['app_id'] = $this->config->item('fbapp_id');
 		
 		$redirect_uri = current_url();	 
		$redirect_string = "";
 
		// if code request is available 
		if(isset($_REQUEST['code']))
		{			 
			 
			$code = $_REQUEST['code'];
			if($this->session->userdata('fb_state') === $_REQUEST['state']) 
			{
				if(isset($_REQUEST['code']))
				{
					$code = $_REQUEST['code'];

					$token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $fb['app_id'] . "&redirect_uri=" . urlencode($redirect_uri)."&client_secret=" . $fb['fbsecret_id'] . "&code=" . $code;
					$response = file_get_contents($token_url); 
					parse_str($response);
					
					$graph_url = "https://graph.facebook.com/me?access_token=" . $access_token;
					
					$user = json_decode(file_get_contents($graph_url));

					$account = $this->account_model->get_all(array('acc_fb_id' => $user->id)); 
					if($account->num_rows() > 0)
					{
						$account = (array) $account->row(); 
					}
					else
					{
						$account_id = "";
						$duplicate_account = $this->account_model->get_by_username($user->email);
						if ($duplicate_account) {
							 $new_account = array( 
							 		"acc_id"		=> $duplicate_account->acc_id,
									"acc_fb_id"		=> $user->id 
								);
							$this->account_model->update($new_account);
							$account_id = $duplicate_account->acc_id;
						} 
						else 
						{
							$new_account = array(
									"acc_username"  => $user->email,
									"acc_first_name" => $user->first_name,
									"acc_last_name" => $user->last_name,
									"acc_fb_id"		=> $user->id, 
									"acc_type" 		=> "user", 
								);

							$fb_getImg = 'http://graph.facebook.com/'.$user->id.'/picture?type=large&redirect=false';
							$fb_getImgProfile = 'http://graph.facebook.com/'.$user->id.'/picture?type=large';
							$profile_pic = json_decode(file_get_contents($fb_getImg));  
							$image_name = md5($user->id.format_mysql_datetime()).'.jpg';
							$facebook_img = file_get_contents($profile_pic->data->url);
 
							$content = file_put_contents("uploads/images/users/".$image_name, $facebook_img);
							$new_account['acc_photo'] = $fb_getImgProfile;  
							$account_id = $this->account_model->create($new_account);
							$ch = curl_init(site_url("grab_news/index/" . $new_account['acc_fb_id']));  

							curl_exec($ch);
							curl_close($ch); 
						}

						$account = $this->account_model->get_one($account_id);
						$account = (array) $account;						
					}

					$this->session->set_userdata("acc_id", $account["acc_id"]);
					$this->session->set_userdata("acc_fb_id", $account["acc_fb_id"]);
					$this->session->set_userdata("acc_fb_token", $account["acc_fb_token"]);
					$this->session->set_userdata("acc_type", $account["acc_type"]);
					$this->session->set_userdata("acc_username",$account["acc_username"]);
					$this->session->set_userdata("acc_first_name",$account["acc_first_name"]);
					$this->session->set_userdata("acc_last_name",$account["acc_last_name"]);
					$this->session->set_userdata("acc_fullname",$account["acc_first_name"].' '.$account["acc_last_name"]);
					$this->session->set_userdata("access_token", $access_token);					
				}		
			}

			redirect('friends');
		}
		elseif(isset($_REQUEST['error']))
		{
			$this->template->notification($_REQUEST['error_description'], 'error');
			redirect();
		}
			

		// call facebook login dialog

		$rand_str = md5(uniqid(rand(), TRUE));
		$this->session->set_userdata('fb_state', $rand_str); // CSRF protection
		$fb_url_connect = "https://www.facebook.com/dialog/oauth?client_id=" . $fb['app_id'] . "&redirect_uri=" . urlencode($redirect_uri) . "&response_type=code&state=" . $this->session->userdata('fb_state'). "&scope=email,user_birthday,user_photos,read_stream";
		
		// redirect page to new loc
		redirect($fb_url_connect);
	}
}
