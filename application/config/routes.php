<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

// Set the default controller of the site.
$route['default_controller'] = "site/sign_in/index";
$route['logout'] = "site/sign_in/logout";


/*
Redirect all other pages to the site folder. 

No need to add the "site" folder when creating links.

Example:
http://www.mysite.com/signup/newuser will point to your site/signup controller 
and will call the newuser() function.

Always keep this at the bottom of this list (catch-all routing rule).
*/
$route['(:any)'] = "site/$1";

// Redirect all 404 errors to this controller.
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
